import core.DriverSetup;
import core.testrail.TestRail;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import pages.MainPage;
import pages.SignUpPage;

import java.io.IOException;

public class MainTests {
    WebDriver driver = DriverSetup.getDriverInstance().getDriver();
    TestRail testRail = TestRail.getTestRailClientInstance();
    MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
    SignUpPage signUpPage = PageFactory.initElements(driver, SignUpPage.class);

    @BeforeSuite
    public void CreateTestRailRun() {
        testRail.createNewRun();
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void mainPageAccessibility() throws InterruptedException {
        testRail.isTestCaseN("C1");
        mainPage.openSiteMainPage("https://www.med-test.in.ua/");
        mainPage.successOfMainPageOpening();
    }

    @AfterMethod
    public void setStatus(ITestResult result) {
        testRail.setResultForCase(result);
    }

    @AfterSuite
    public void cleanUp() {
        testRail.closeRun();
        driver.quit();
        try {
            Runtime.getRuntime().exec("allure serve target/allure-results");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
