import core.enums.Languages;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class RegistrationFormTests extends MainTests {

    @Test(priority = 1)
    @Severity(SeverityLevel.CRITICAL)
    public void signUpFormAccessibility() {
        testRail.isTestCaseN("C2");
        mainPage.openSiteMainPage("https://www.med-test.in.ua/");
        mainPage.selectLanguages(Languages.ENGLISH);
        mainPage.clickMenuButtonSignUp();
        signUpPage.successOfOpeningSignUpForm();
    }

    @Test(priority = 2)
    @Severity(SeverityLevel.CRITICAL)
    public void signUpFormNotAccessible() {
        testRail.isTestCaseN("C22");
        mainPage.openSiteMainPage("https://www.med-test.in.ua/");
        mainPage.selectLanguages(Languages.ENGLISH);
        signUpPage.successOfOpeningSignUpForm();
    }

    @DataProvider(name = "badEmailDataWithMessages")
    public Object[][] badEmailDataWithMessages() {
        return new Object[][]{
                {"daasdadsa", "Please include an '@' in the email address. '%s' is missing an '@'."},
                {"%", "Please include an '@' in the email address. '%s' is missing an '@'."},
                {"testemail", "Please include an '@' in the email address. '%s' is missing an '@'."},
                {"kakaha@", "Please enter a part following '@'. '%s' is incomplete."},
                {"@", "Please enter a part following '@'. '%s' is incomplete."},
                {"@32", "Please enter a part followed by '@'. '%s' is incomplete."},
        };
    }

    @Test(dataProvider = "badEmailDataWithMessages")
    @Severity(SeverityLevel.NORMAL)
    public void checkNotValidDataOfEmailField(String text, String errorMessage) {
        testRail.isTestCaseN("C3");
        mainPage.openSiteMainPage("https://www.med-test.in.ua/");
        mainPage.selectLanguages(Languages.ENGLISH);
        mainPage.clickMenuButtonSignUp();
        signUpPage.successOfOpeningSignUpForm();
        signUpPage.inputEmail(text);
        signUpPage.clickSignUpButton();
        signUpPage.checkErrorMessageOfEmailField(String.format(errorMessage, text));
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    public void checkNotValidDataOfEmailField() {
        testRail.isTestCaseN("C3");
        mainPage.openSiteMainPage("https://www.med-test.in.ua/");
        mainPage.selectLanguages(Languages.ENGLISH);
        mainPage.clickMenuButtonSignUp();
        signUpPage.successOfOpeningSignUpForm();
        signUpPage.inputEmail("kakaha@ne.");
        signUpPage.clickSignUpButton();
        signUpPage.checkErrorMessageOfEmailField("'.' is used at a wrong position in 'ne.'.");
    }

    @DataProvider(name = "badPasswordData")
    public Object[][] badPasswordData() {
        return new Object[][]{
                {"12", ""},
                {"", "12"},
                {"adcdddttt", "adcdddtt"},
        };
    }

    @Test(dataProvider = "badPasswordData")
    @Severity(SeverityLevel.NORMAL)
    public void checkErrorOfPasswordConfirmation(String pass, String confirmPass) {
        testRail.isTestCaseN("C4");
        mainPage.openSiteMainPage("https://www.med-test.in.ua/");
        mainPage.selectLanguages(Languages.ENGLISH);
        mainPage.clickMenuButtonSignUp();
        signUpPage.successOfOpeningSignUpForm();
        signUpPage.inputEmail("test@test.com");
        signUpPage.inputPassword(pass);
        signUpPage.inputConfirmPassword(confirmPass);
        signUpPage.clickSignUpButton();
        signUpPage.checkErrorMessage("The passwords do not confirm");
    }

    @DataProvider(name = "notValidPasswordData")
    public Object[][] notValidPasswordData() {
        return new Object[][]{
                {"12"},
                {"adc"},
                {"123456789"},
                {"a1"},
                {"a1234"},
        };
    }

    @Test(dataProvider = "notValidPasswordData")
    @Severity(SeverityLevel.NORMAL)
    public void checkErrorOfPasswordValidation(String pass) {
        testRail.isTestCaseN("C5");
        mainPage.openSiteMainPage("https://www.med-test.in.ua/");
        mainPage.selectLanguages(Languages.ENGLISH);
        mainPage.clickMenuButtonSignUp();
        signUpPage.successOfOpeningSignUpForm();
        signUpPage.inputEmail("test@test.com");
        signUpPage.inputPassword(pass);
        signUpPage.inputConfirmPassword(pass);
        signUpPage.clickSignUpButton();
        signUpPage.checkErrorMessage("Your password mast have minimum 1 digit, 1 char with lower or " +
                "uppercase and length more than 6 characters");
    }
}