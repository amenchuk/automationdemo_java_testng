package core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class DriverSetup {

    private static DriverSetup driverInstance = null;
    private static WebDriver driver;

    private DriverSetup() {
        System.setProperty("webdriver.chrome.driver", "./lib/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    public static DriverSetup getDriverInstance() {
        if (driverInstance == null)
            driverInstance = new DriverSetup();
        return driverInstance;
    }

    public WebDriver getDriver() {
        return driver;
    }

}
