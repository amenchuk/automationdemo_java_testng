package core;

import core.enums.Languages;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class Extensions {

    public WebDriver driver;
    public Locale uaLoc = new Locale("uk", "UA");
    private WebDriverWait wait;
    private String className = this.getClass().getSimpleName();

    public Extensions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
    }

    protected void waitForPageLoading() {
        wait.until(
                result ->
                        ((JavascriptExecutor) driver)
                                .executeScript("return document.readyState").toString().equals("complete"));
    }

    protected void waitForElementWillClickable(WebElement element) {
        addLogMessage(className, "Waiting when element will be clickable.");
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected void waitForElementWillVisible(WebElement element) {
        addLogMessage(className, "Waiting when element will be visible.");
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    protected void clickOnButton(WebElement button) {
        addLogMessage(className, "Button will be clicked.");
        waitForElementWillVisible(button);
        waitForElementWillClickable(button);
        button.click();
        addLogMessage(className, "Button was clicked.");
    }

    protected void select(WebElement select, Languages language) {
        waitForElementWillVisible(select);
        waitForElementWillClickable(select);
        clickOnButton(select.findElement(By.xpath(String.format("//li[@data-option-index='0']", language.getLanguageCode()))));
    }

    protected void inputTextToField(WebElement field, String text) {
        addLogMessage(className, String.format("The text [%s] will be inputted.", text));
        waitForElementWillVisible(field);
        waitForElementWillClickable(field);
        field.clear();
        field.sendKeys(text);
        addLogMessage(className, String.format("The text [%s] was inputted.", text));
    }

    protected String getTextFromElement(WebElement element) {
        addLogMessage(className, "Text will be gotten from element.");
        waitForElementWillVisible(element);
        waitForElementWillClickable(element);
        return element.getText();
    }

    protected void addLogMessage(String className, String message) {
        System.out.println(String.format("[%s] %s: %s",
                DateFormat.getDateTimeInstance().format(new Date()),
                className,
                message));
    }
}