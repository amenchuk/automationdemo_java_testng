package core.enums;

public enum TestRailRoutes {
    ADD_RUN_URL("add_run/"),
    CLOSE_RUN_URL("close_run/"),
    ADD_TEST_RESULT("add_result_for_case/");

    private final String route;

    TestRailRoutes(String route) {
        this.route = route;
    }

    public String getRoute() {
        return this.route;
    }
}
