package core.enums;

public enum Languages {
    ENGLISH("0"),
    RUSSIAN("1"),
    UKRAINIAN("2");

    private final String languageCode;

    Languages(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getLanguageCode() {
        return this.languageCode;
    }
}