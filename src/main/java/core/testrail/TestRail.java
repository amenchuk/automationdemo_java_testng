package core.testrail;

import org.json.simple.JSONObject;
import org.testng.ITestResult;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static core.enums.TestRailRoutes.*;

public class TestRail {

    private static TestRail trClientInstance = null;
    private static TestRailAPIClient client;
    private String baseURL = "https://qarecruite.testrail.io/";
    private String userName = "x8rxso1@seomail.top";
    private String userPassword = "LbQOxw50nuFBOA66Z5LG";
    private int projectId = 1;
    private int runId;
    private String testId;

    private TestRail() {
        client = new TestRailAPIClient(baseURL);
        client.setUser(userName);
        client.setPassword(userPassword);
    }

    public static TestRail getTestRailClientInstance() {
        if (trClientInstance == null)
            trClientInstance = new TestRail();
        return trClientInstance;
    }

    public TestRailAPIClient getTestRailClient() {
        return client;
    }

    public void createNewRun() {
        Map data = new HashMap();
        data.put("name", "Test Run at " + new SimpleDateFormat("HH:mm:SS yyyy-MM-dd").format(new Date()));
        data.put("include_all", true);
        JSONObject r = null;
        try {
            r = (JSONObject) client.sendPost(String.format("%s%d", ADD_RUN_URL.getRoute(), projectId), data);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TestRailAPIException e) {
            e.printStackTrace();
        }
        runId = Integer.parseInt(r.get("id").toString());
    }

    public void closeRun() {
        JSONObject r = null;
        try {
            r = (JSONObject) client.sendPost(String.format("%s%d", CLOSE_RUN_URL.getRoute(), runId), new HashMap());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TestRailAPIException e) {
            e.printStackTrace();
        }
    }

    public void setResultForCase(ITestResult result) {
        int status = 4;
        switch (result.getStatus()) {
            case ITestResult.SUCCESS:
                status = 1;
                break;
            case ITestResult.FAILURE:
                status = 5;
                break;
            case ITestResult.SKIP:
                status = 4;
                break;
        }
        StringBuilder comment = new StringBuilder();
        comment.append("\nMethod name: ").append(result.getName());
        if (result.getParameters() != null) {
            comment.append("\nParameters: ");
            for (int i = 0; i < result.getParameters().length; i++) {
                comment.append("[").append(result.getParameters()[i].toString()).append("]");
                if (i != result.getParameters().length - 1) comment.append(", ");
            }
        }
        if (result.getThrowable() != null)
            comment.append("\nException : " + result.getThrowable().getMessage());

        Map data = new HashMap();
        data.put("status_id", status);
        data.put("comment", comment.toString());
        JSONObject r = null;
        try {
            r = (JSONObject) client.sendPost(String.format("%s%d/%s", ADD_TEST_RESULT.getRoute(), runId, testId), data);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TestRailAPIException e) {
            e.printStackTrace();
        }
    }

    public void isTestCaseN(String testCaseNumber) {
        testId = testCaseNumber.replaceAll("C", "");
    }
}
