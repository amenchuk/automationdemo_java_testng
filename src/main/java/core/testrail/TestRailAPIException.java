/**
 * TestRail API binding for Java (API v2, available since TestRail 3.0)
 * <p>
 * Learn more:
 * <p>
 * http://docs.gurock.com/testrail-api2/start
 * http://docs.gurock.com/testrail-api2/accessing
 * <p>
 * Copyright Gurock Software GmbH. See license.md for details.
 */

package core.testrail;

public class TestRailAPIException extends Exception {
    public TestRailAPIException(String message) {
        super(message);
    }
}
