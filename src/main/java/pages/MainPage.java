package pages;

import core.Extensions;
import core.enums.Languages;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class MainPage extends Extensions {

    private String className = this.getClass().getSimpleName();
    @FindBy(xpath = "//div[contains(@class,'green-box')]/div[1]//h4") private WebElement textOfLike;
    @FindBy(xpath = "//a[@href='/signup']") private WebElement buttonSignUpMenuButton;
    @FindBy(xpath = "//div[@id='ember459']") private WebElement selectFieldLanguages;
    @FindBy(xpath = "//div[contains(@id,'dropdown-content-ember469')]") private WebElement selectLanguages;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    @Step("Open site: {stringURL}")
    public void openSiteMainPage(String stringURL) {
        addLogMessage(className, String.format("Try to open [%s]", stringURL));
        driver.get(stringURL);
        waitForPageLoading();
    }

    @Step("Click \"SignUp\" button at the menu line")
    public void clickMenuButtonSignUp() {
        waitForPageLoading();
        addLogMessage(className, "Click on the SignUp button.");
        clickOnButton(buttonSignUpMenuButton);
        waitForPageLoading();
    }

    @Step("Select language: {language}")
    public void selectLanguages(Languages language) {
        waitForPageLoading();
        clickOnButton(selectFieldLanguages);
        select(selectLanguages, language);
        waitForPageLoading();
    }

    @Step("Approve the success of opening main page of the site.")
    public void successOfMainPageOpening() {
        waitForPageLoading();
        addLogMessage(className, "Asserting of the success page loading.");
        Assert.assertTrue(textOfLike.isDisplayed(), "The text from the main page is not displayed.");
    }
}