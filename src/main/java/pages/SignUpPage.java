package pages;

import core.Extensions;
import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class SignUpPage extends Extensions {

    private String className = this.getClass().getSimpleName();
    @FindBy(xpath = "//form[@class='form-signup']")
    private WebElement formSignUp;
    @FindBy(xpath = "//input[@type='email']")
    private WebElement inputFieldLogin;
    @FindBy(xpath = "//input[@id='input-password']")
    private WebElement inputFieldPassword;
    @FindBy(xpath = "//input[@id='password-confirmation']")
    private WebElement inputFieldConfirmPassword;
    @FindBy(xpath = "//button[@type='submit']")
    private WebElement buttonSignUp;
    @FindBy(xpath = "//div[contains(@class,'flash-message')]")
    private WebElement errorMessageBox;

    public SignUpPage(WebDriver driver) {
        super(driver);
    }

    @Step("Input [{login}] into the \"Email\" field")
    public void inputEmail(String login) {
        waitForPageLoading();
        inputTextToField(inputFieldLogin, login);
    }

    @Step("Input [{password}] into the \"Password\" field")
    public void inputPassword(String password) {
        waitForPageLoading();
        inputTextToField(inputFieldPassword, password);
    }

    @Step("Input [{password}] into the \"Confirm password\" field")
    public void inputConfirmPassword(String password) {
        waitForPageLoading();
        inputTextToField(inputFieldConfirmPassword, password);
    }

    @Step("Click on the \"SignUp\" button")
    public void clickSignUpButton() {
        waitForPageLoading();
        clickOnButton(buttonSignUp);
    }

    @Step("Approve the success of opening \"SignUp\" form")
    public void successOfOpeningSignUpForm() {
        try {
            waitForPageLoading();
            addLogMessage(className, "Asserting of the success opening of \"Sign Up\" form.");
            Assert.assertTrue(formSignUp.isDisplayed());
        }catch (NoSuchElementException e){
            Assert.fail();
        }
    }

    @Step("Checking of displaying error message: [{expectedErrorMessage}] at the \"Email\" field")
    public void checkErrorMessageOfEmailField(String expectedErrorMessage) {
        waitForPageLoading();
        addLogMessage(className, String.format("Check that error message with text [%s] is displayed.", expectedErrorMessage));
        String currentErrorMessage =
                (String) ((JavascriptExecutor) driver)
                        .executeScript("return arguments[0].validationMessage", inputFieldLogin);
        Assert.assertEquals(currentErrorMessage, expectedErrorMessage);
    }

    @Step("Checking of displaying error message: [{expectedErrorMessage}]")
    public void checkErrorMessage(String expectedErrorMessage) {
        waitForPageLoading();
        Assert.assertEquals(getTextFromElement(errorMessageBox), expectedErrorMessage);
    }
}